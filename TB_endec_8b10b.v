`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company:     Z-Control
// Engineer:    Lu Shengkang
//
// Create Date:   10:46:16 02/25/2019
// Design Name:   dec_8b10b
// Module Name:   
// Project Name:  
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: dec_8b10b
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module TB_endec_8b10b;
	
	
	// Inputs
	reg clk;
	reg rst;
	reg [9:0] data_in_10b;

	// Outputs
	wire KO;
	wire [7:0] data_out_8b;
	
	// Inputs
	//reg clk;
	//reg rst;
	reg KI;
	reg [7:0] data_in_8b;

	// Outputs
	wire [9:0] data_out_10b;

	// Instantiate the Unit Under Test (UUT)
	enc_8b10b uut1 (
		.clk(clk), 
		.rst(rst), 
		.KI(KI), 
		.data_in_8b(data_in_8b), 
		.data_out_10b(data_out_10b)
	);
	
	
	// Instantiate the Unit Under Test (UUT)
	dec_8b10b uut2 (
		.clk(clk), 
		.rst(rst), 
		.data_in_10b(data_in_10b), 
		.KO(KO), 
		.data_out_8b(data_out_8b)
	);
	
	//assign	data_in_10b	=	data_out_10b;
	always	@(*)
	begin
		data_in_10b	=	data_out_10b;
	end	
	/*	//测试间隔接收到的10b编码是否能够正常解码
	reg	[1:0]	cnt;
	always	@(posedge	clk)
	begin
		if(rst)begin
			cnt	<=	'b0;
		end
		else	begin
			cnt	<=	cnt	+	1'b1;
		end
	end	
	
	always	@(posedge	clk)
	begin
		if(rst)begin
			data_in_10b	<=	'b0;
		end
		else	if(cnt=='d2)	begin
			data_in_10b	<=	data_out_10b;
		end
		else	begin
			data_in_10b	<=	'b0;
		end
	end	
	*/
	
	//-- Special character code values
	reg	[7:0]	K28d0	=	'b0001_1100;	//-- Balanced  
	reg	[7:0]	K28d1	=	'b0011_1100;	//-- Unbalanced comma 
	reg	[7:0]	K28d2	=	'b0101_1100;	//-- Unbalanced 
	reg	[7:0]	K28d3	=	'b0111_1100;	//-- Unbalanced 
	reg	[7:0]	K28d4	=	'b1001_1100;	//-- Balanced  
	reg	[7:0]	K28d5	=	'b1011_1100;	//-- Unbalanced comma 
	reg	[7:0]	K28d6	=	'b1101_1100;	//-- Unbalanced 
	reg	[7:0]	K28d7	=	'b1111_1100;	//-- Balanced comma 
	reg	[7:0]	K23d7	=	'b1111_0111;	//-- Balanced
	reg	[7:0]	K27d7	=	'b1111_1011;	//-- Balanced
	reg	[7:0]	K29d7	=	'b1111_1101;	//-- Balanced
	reg	[7:0]	K30d7	=	'b1111_1110;	//-- Balanced
	
	//-- Signals for TestBench control functions
	reg	[7:0]	tchar;					//-- All character vector
	reg	[3:0]	kcounter;				//-- K character counter
	reg	[7:0]	dcounter;				//-- D value counter
	//reg	[9:0]	tcharout,tlcharout;	//-- Character output vector
	reg			tclken;					//-- Enables clock after short delay starting up
	reg			tcnten;					//-- Enables count after 1 cycle
	reg			tks;						//-- Use to select control function of encoder
	reg			dk;						//-- '0' if D, '1' if K

	always	@(*)
	begin
		if(rst==1)begin
			tcnten <= 'b0 ;
		end
		else	begin
			tcnten <= 'b1 ;
		end
	end
	
	always	@(posedge	clk)
	begin
		if(rst==1)begin
			tchar		<=	'b0000_0000;
			tks		<=	'b1;
			dk			<=	'b0;
			kcounter	<=	'b0000;
			dcounter	<=	'b0000_0000;
		end
		else	begin
			dk	<=	tks;
			if(tks==1)begin
				kcounter	<=	kcounter	+	tcnten;
				dcounter	<=	'b0000_0000;
				case(kcounter)
					'b0000:	tchar	<=	K28d0;
					'b0001:	tchar	<=	K28d1;
					'b0010:	tchar	<=	K28d2;
					'b0011:	tchar	<=	K28d3;
					'b0100:	tchar	<=	K28d4;
					'b0101:	tchar	<=	K28d5;
					'b0110:	tchar	<=	K28d6;
					'b0111:	tchar	<=	K28d7;					
					'b1000:	tchar	<=	K23d7;
					'b1001:	tchar	<=	K27d7;
					'b1010:	tchar	<=	K29d7;
					'b1011:	begin 
								tchar	<=	K30d7;
								tks	<=	'b0;
								end
					'b1100:	tchar	<=	'b0000;
					default:	tchar	<=	K28d5;
				endcase
			end
			else	begin
				dcounter	<=	dcounter	+	tcnten;
				tchar	<=	dcounter;
				if(dcounter=='b1111_1111)begin
					tks	<=	'b1;
					kcounter	<=	'b0000;
				end
			end
		end
	end
	
	always	@(*)
	begin
		data_in_8b	=	tchar;
		KI			=	dk;
	end
	
	always	#50	clk=~clk;

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 1;
		KI = 0;
		data_in_8b = 0;		
		tcnten  = 0;
		tclken  = 0;
		tks	  = 1;
		dk		  = 0;
		#10
		tclken  = 1;
		// Wait 100 ns for global reset to finish
		#190;
		rst = 0;
		// Add stimulus here
	end
      
endmodule

