`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company:     Z-Control
// Engineer:    Lu Shengkang
//
// Create Date:   13:24:28 02/22/2019
// Design Name:   enc_8b10b
// Module Name:   
// Project Name:  
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: enc_8b10b
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module TB_enc_8b10b;

	// Inputs
	reg clk;
	reg rst;
	reg KI;
	reg [7:0] data_in_8b;

	// Outputs
	wire [9:0] data_out_10b;

	// Instantiate the Unit Under Test (UUT)
	enc_8b10b uut (
		.clk(clk), 
		.rst(rst), 
		.KI(KI), 
		.data_in_8b(data_in_8b), 
		.data_out_10b(data_out_10b)
	);
	
	//-- Special character code values
	reg	[7:0]	K28d0	=	'b0001_1100;	//-- Balanced  
	reg	[7:0]	K28d1	=	'b0011_1100;	//-- Unbalanced comma 
	reg	[7:0]	K28d2	=	'b0101_1100;	//-- Unbalanced 
	reg	[7:0]	K28d3	=	'b0111_1100;	//-- Unbalanced 
	reg	[7:0]	K28d4	=	'b1001_1100;	//-- Balanced  
	reg	[7:0]	K28d5	=	'b1011_1100;	//-- Unbalanced comma 
	reg	[7:0]	K28d6	=	'b1101_1100;	//-- Unbalanced 
	reg	[7:0]	K28d7	=	'b1111_1100;	//-- Balanced comma 
	reg	[7:0]	K23d7	=	'b1111_0111;	//-- Balanced
	reg	[7:0]	K27d7	=	'b1111_1011;	//-- Balanced
	reg	[7:0]	K29d7	=	'b1111_1101;	//-- Balanced
	reg	[7:0]	K30d7	=	'b1111_1110;	//-- Balanced

	/*
	//-- Stimulus signals - mapped to the input  of enc_8b10b
	reg	TKO;
	reg	TAO;
	reg	TBO;
	reg	TCO;
	reg	TDO;
	reg	TEO;
	reg	TFO;
	reg	TGO;
	reg	THO;
	
	//-- Observed signals - mapped from output of enc_8b10b
	reg	TA;
	reg	TB;
	reg	TC;
	reg	TD;
	reg	TE;
	reg	TF;
	reg	TI;
	reg	TG;
	reg	TH;
	reg	TJ;
*/
/*	
	assign	KI				=	TKO;
	assign	data_in[0]	=	TAO;
	assign	data_in[1]	=	TBO;
	assign	data_in[2]	=	TCO;
	assign	data_in[3]	=	TDO;
	assign	data_in[4]	=	TEO;
	assign	data_in[5]	=	TFO;
	assign	data_in[6]	=	TGO;
	assign	data_in[7]	=	THO;
*/

	//-- Signals for TestBench control functions
	reg	[7:0]	tchar;					//-- All character vector
	reg	[3:0]	kcounter;				//-- K character counter
	reg	[7:0]	dcounter;				//-- D value counter
	//reg	[9:0]	tcharout,tlcharout;	//-- Character output vector
	reg			tclken;					//-- Enables clock after short delay starting up
	reg			tcnten;					//-- Enables count after 1 cycle
	reg			tks;						//-- Use to select control function of encoder
	reg			dk;						//-- '0' if D, '1' if K

	always	@(*)
	begin
		if(rst==1)begin
			tcnten <= 'b0 ;
		end
		else	begin
			tcnten <= 'b1 ;
		end
	end
	
	always	@(posedge	clk)
	begin
		if(rst==1)begin
			tchar		<=	'b0000_0000;
			tks		<=	'b1;
			dk			<=	'b0;
			kcounter	<=	'b0000;
			dcounter	<=	'b0000_0000;
		end
		else	begin
			dk	<=	tks;
			if(tks==1)begin
				kcounter	<=	kcounter	+	tcnten;
				dcounter	<=	'b0000_0000;
				case(kcounter)
					'b0000:	tchar	<=	K28d0;
					'b0001:	tchar	<=	K28d1;
					'b0010:	tchar	<=	K28d2;
					'b0011:	tchar	<=	K28d3;
					'b0100:	tchar	<=	K28d4;
					'b0101:	tchar	<=	K28d5;
					'b0110:	tchar	<=	K28d6;
					'b0111:	tchar	<=	K28d7;					
					'b1000:	tchar	<=	K23d7;
					'b1001:	tchar	<=	K27d7;
					'b1010:	tchar	<=	K29d7;
					'b1011:	begin 
								tchar	<=	K30d7;
								tks	<=	'b0;
								end
					'b1100:	tchar	<=	'b0000;
					default:	tchar	<=	K28d5;
				endcase
			end
			else	begin
				dcounter	<=	dcounter	+	tcnten;
				tchar	<=	dcounter;
				if(dcounter=='b1111_1111)begin
					tks	<=	'b1;
					kcounter	<=	'b0000;
				end
			end
		end
	end
/*	
	//-- Latch encoder output each rising edge for simulation display
	always	@(posedge	clk)
	begin
		tlcharout[0]	<=	TA;
		tlcharout[1]	<=	TB;
		tlcharout[2]	<=	TC;
		tlcharout[3]	<=	TD;
		tlcharout[4]	<=	TE;
		tlcharout[5]	<=	TF;
		tlcharout[6]	<=	TG;
		tlcharout[7]	<=	TH;		
	end
*/
	always	@(*)
	begin
		data_in_8b	=	tchar;
		KI			=	dk;
	end

/*	
	assign	TAO	=	tchar[0];
	assign	TBO	=	tchar[1];
	assign	TCO	=	tchar[2];
	assign	TDO	=	tchar[3];
	assign	TEO	=	tchar[4];
	assign	TFO	=	tchar[5];
	assign	TGO	=	tchar[6];
	assign	THO	=	tchar[7];
	assign	TKO	=	dk;
	
	assign	tcharout[0]	=	TA;
	assign	tcharout[1]	=	TB;
	assign	tcharout[2]	=	TC;
	assign	tcharout[3]	=	TD;
	assign	tcharout[4]	=	TE;
	assign	tcharout[5]	=	TI;
	assign	tcharout[6]	=	TF;
	assign	tcharout[7]	=	TG;
	assign	tcharout[8]	=	TH;
	assign	tcharout[9]	=	TJ;		
*/
		
	always	#50	clk=~clk;
	
	
	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 1;
		KI = 0;
		data_in_8b = 0;		
		tcnten  = 0;
		tclken  = 0;
		tks	  = 1;
		dk		  = 0;
		#10
		tclken  = 1;
		// Wait 100 ns for global reset to finish
		#190;
		rst = 0;
		// Add stimulus here

	end
      
endmodule

