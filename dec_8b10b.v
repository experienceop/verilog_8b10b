`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:     Z-Control
// Engineer:    Lu Shengkang
// 
// Create Date:    08:58:56 02/25/2019 
// Design Name: 
// Module Name:    dec_8b10b 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module dec_8b10b(
	input		wire			clk,
	input		wire			rst,
	input		wire	[9:0]	data_in_10b,
	output	reg			KO,
	output	reg	[7:0]	data_out_8b
    );
	
	wire	AI	=	data_in_10b[0];
	wire	BI	=	data_in_10b[1];
	wire	CI	=	data_in_10b[2];
	wire	DI	=	data_in_10b[3];
	wire	EI	=	data_in_10b[4];
	wire	II	=	data_in_10b[5];
	wire	FI	=	data_in_10b[6];
	wire	GI	=	data_in_10b[7];
	wire	HI	=	data_in_10b[8];
	wire	JI	=	data_in_10b[9];
		
	//-- Signals to tie things together
	//reg	ANEB, CNED, EEI, P13, P22, P31; 						 //: std_logic ;	-- Figure 10 Signals
	//reg	IKA, IKB, IKC; 										    //: std_logic ;	-- Figure 11 Signals
	//reg	XA, XB, XC, XD, XE;				  						 //: std_logic ;-- Figure 12 Signals
	//reg	OR121, OR122, OR123, OR124, OR125, OR126, OR127; //: std_logic 
	//reg	XF, XG, XH;							  						 //: std_logic ;	-- Figure 13 Signals 
	//reg	OR131, OR132, OR133, OR134, IOR134; 				 //: std_logic ; 
	reg	AO,BO,CO,DO,EO,FO,GO,HO;
		
	//--
	//-- 6b Input Function (Reference: Figure 10)
	//--
	
	//-- One 1 and three 0's
	wire	P13 =	(ANEB & (! CI & ! DI))
					| (CNED & (! AI & ! BI)) ;
	//-- Three 1's and one 0		
	wire	P31 =	(ANEB & CI & DI)
					| (CNED & AI & BI) ;
	//-- Two 1's & two 0's
	wire	P22 =	(AI & BI & (! CI & ! DI))
					| (CI & DI & (! AI & ! BI))
					| (ANEB & CNED) ;
	
	//-- Intermediate term for "AI is Not Equal to BI"
	wire	ANEB =  AI ^	BI ;
	
	//-- Intermediate term for "CI is Not Equal to DI"
	wire	CNED =  CI ^	DI ;
	
	//-- Intermediate term for "E is Equal to I"
	wire	EEI = EI ^~ II ;
	
	//--
	//-- K Decoder - Figure 11
	//--
	
	//-- Intermediate terms
	wire	IKA = (CI & DI & EI & II)
					| (! CI & ! DI & ! EI & ! II) ;
	wire	IKB = P13 &(! EI & II & GI & HI & JI) ;
	wire	IKC = P31 & (EI & ! II & ! GI & ! HI & ! JI) ;
	
	//-- PROCESS: KFN; Determine K output
	always	@(posedge	clk)
	begin
		if(rst)begin
			KO	<=	'b0;
		end
		else	begin
			KO	<=	IKA | IKB | IKC;
		end
	end
	
	//--
	//-- 5b Decoder Figure 12
	//--
	
	//-- Logic to determine complimenting A,B,C,D,E,I inputs
	wire	OR121 = (P22 & (! AI & ! CI & EEI))
						| (P13 & ! EI) ;
	wire	OR122 = (AI & BI & EI & II)
						| (! CI & ! DI & ! EI & ! II)
						| (P31 & II) ;
	wire	OR123 = (P31 & II)
						| (P22 & BI & CI & EEI)
						| (P13 & DI & EI & II) ;
	wire	OR124 = (P22 & AI & CI & EEI)
						| (P13 & ! EI) ;
	wire	OR125 = (P13 & ! EI)
						| (! CI & ! DI & ! EI & ! II)
						| (! AI & ! BI & ! EI & ! II) ;
	wire	OR126 = (P22 & ! AI & ! CI & EEI)
						| (P13 & ! II) ;
	wire	OR127 = (P13 & DI & EI & II)
						| (P22 & ! BI & ! CI & EEI) ;
	
	wire	XA = OR127
					| OR121
					| OR122 ;
	wire	XB = OR122
					| OR123
					| OR124 ;
	wire	XC = OR121
					| OR123
					| OR125 ;
	wire	XD = OR122
					| OR124
					| OR127 ;
	wire	XE = OR125
					| OR126
					| OR127 ; 
					
	//-- PROCESS: DEC5B; Generate and latch LS 5 decoded bits
	always	@(negedge	clk)
	begin
		if(rst)begin
			AO <= 'b0 ;
			BO <= 'b0 ;
			CO <= 'b0 ;
			DO <= 'b0 ;
			EO <= 'b0 ;
		end
		else	begin
			AO <= XA ^ AI ;	//-- Least significant bit 0
			BO <= XB ^ BI ;
			CO <= XC ^ CI ;
			DO <= XD ^ DI ;
			EO <= XE ^ EI ;	//-- Most significant bit 6
		end
	end
	
	//--
	//-- 3b Decoder - Figure 13
	//--
	
	//-- Logic for complimenting F,G,H outputs
	wire	OR131 = (GI & HI & JI)
						| (FI & HI & JI) 
						| (IOR134);
	wire	OR132 = (FI & GI & JI)
						| (! FI & ! GI & ! HI)
						| (! FI & ! GI & HI & JI);
	wire	OR133 = (! FI & ! HI & ! JI)
						| (IOR134)
						| (! GI & ! HI & ! JI) ;
	wire	OR134 = (! GI & ! HI & ! JI)
						| (FI & HI & JI)
						| (IOR134) ;
	wire	IOR134 = (! (HI & JI))
						& (! (! HI & ! JI))
						& (! CI & ! DI & ! EI & ! II) ;
	
	wire	XF = OR131
					| OR132 ;
	wire	XG = OR132 
					| OR133 ;
	wire	XH = OR132
					| OR134 ;
					
	//-- PROCESS: DEC3B; Generate and latch MS 3 decoded bits
	always	@(negedge	clk)
	begin
		if(rst)begin
			FO <= 'b0 ;
			GO <= 'b0 ;
			HO <= 'b0 ;
		end
		else	begin
			FO <= XF ^ FI ;	//-- Least significant bit 7
			GO <= XG ^ GI ;
			HO <= XH ^ HI ;	//-- Most significant bit 10
		end
	end	
	
	always	@(posedge	clk)
	begin	
		if(rst)begin
			data_out_8b[0]	<=	'b0;
			data_out_8b[1]	<=	'b0;
			data_out_8b[2]	<=	'b0;
			data_out_8b[3]	<=	'b0;
			data_out_8b[4]	<=	'b0;
			data_out_8b[5]	<=	'b0;
			data_out_8b[6]	<=	'b0;
			data_out_8b[7]	<=	'b0;
		end
		else	begin
			data_out_8b[0]	<=	AO;
			data_out_8b[1]	<=	BO;
			data_out_8b[2]	<=	CO;
			data_out_8b[3]	<=	DO;
			data_out_8b[4]	<=	EO;
			data_out_8b[5]	<=	FO;
			data_out_8b[6]	<=	GO;
			data_out_8b[7]	<=	HO;
		end	
	end	
endmodule
