# verilog_8b10b

#### 介绍
8b10b verilog
Physical layer coding 

#### 支持modelsim仿真
#### 使用说明

1. 编码规则参考——8b10b详解.pdf 
2. 仿真结果可以参考——8B10B编码表.doc
3. 添加仿真文件
    dec_8b10b.v
    enc_8b10b.v
    TB_endec_8b10b.v
    可以查看编解码结果
4. 判断是否为K码需要根据K值，当K为1时，输出为K码

#### 参与贡献

1. 参考来源opencores VHDL源码
2. cloud lu
